#!/usr/bin/env bash

# ==== README ================================================================ #

# shellcheck disable=SC1091
#	- allow sourcing external files on line 10

# ==== SOURCES =============================================================== #

[ -f /etc/bashrc ] && . /etc/bashrc

# ==== EXPORTS =============================================================== #

export PS1="\[\e[92m\]\W: \[\e[0m\]"

# ==== ALIASES =============================================================== #

alias mv='mv -i'
alias rm='rm -i'

alias dot='cd ${HOME}/.config'
alias mpv='flatpak run io.mpv.Mpv'
alias pdf='zathura'
alias red='nohup wlsunset -t 3000 -T 6500 -l 35 -L -120 &'

alias flat='flatpak'
alias kpxc='flatpak run org.keepassxc.KeePassXC'
alias sudo='doas'

alias office='flatpak run org.libreoffice.LibreOffice'
alias yt-aud='yt-dlp -f bestaudio -x --audio-format opus --audio-quality 0'
alias yt-vid='yt-dlp -f bestaudio+bestvideo'

# ==== FUNCTIONS ============================================================= #

# These functions are just aliases that might require root privilege. If
# functions aren't just aliases, then place them in "$HOME"/.local/bin/.

vi()
{
	nvim "$@"
}

vim()
{
	nvim "$@"
}

# ==== EOF =================================================================== #
