#!/usr/bin/env bash

# ==== README ================================================================ #

# shellcheck disable=SC1091
#	- allow sourcing external files on line 10

# ==== SOURCES =============================================================== #

[ -f "${HOME}/.bashrc" ] && . "${HOME}/.bashrc"

# ==== ENVIRONMENT VARIABLES ================================================= #

[ -d "${HOME}/.local/bin" ] && export PATH="${PATH}:${HOME}/.local/bin"

export EDITOR="nvim"
export VISUAL="nvim"

export MOZ_ENABLE_WAYLAND=1

# ==== PROGRAM INITIALIZATION ================================================ #

[ "$(tty)" = "/dev/tty1" ] && exec sway

# ==== EOF =================================================================== #
